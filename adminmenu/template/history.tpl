<div class="panel panel-default">
    <div class="panel-heading">
        <h2 class="panel-title">{__('Vorgangsliste')}</h2>
    </div>
    <div class="panel-body">
        <table class="table">
            <tr>
                <th>HistoryId</th>
                <th>{__('Alte Ust.')}</th>
                <th>{__('Neue Ust.')}</th>
                <th>{__('Bruttopreise beibehalten')}?</th>
                <th>{__('Startzeitpunkt')}</th>
                <th>{__('Endzeitpunkt')}</th>
            </tr>
            {if $history}
                {foreach $history as $data}
                    <tr class="{if $data->endTime}table-success{else}table-danger{/if}">
                        <th>{$data->historyId}</th>
                        <th>{$data->oldVAT}</th>
                        <th>{$data->newVAT}</th>
                        <th>{if $data->keepGrossPrice === '1'}{__('Ja')}{else}{__('Nein')}{/if}</th>
                        <th>{$data->startTime}</th>
                        <th>{$data->endTime}</th>
                    </tr>
                {/foreach}
            {/if}
        </table>
        {if !$history}
            {$alertList->displayAlertByKey('noHistoryData')}
        {/if}
        <p style="margin-top: 20px">
            <span style="color: green">{__('Grün')}: </span><span>{__('Vorgang <strong>NICHT</strong> erfolgreich durchgeführt. Vorgang erfolgreich durchgeführt.')}</span><br>
            <span style="color: red">{__('Rot')}: </span><span>{__('Bitte stoßen Sie das Update an, um den Vorgang abzuschließen')}</span><br>
        </p>
    </div>
</div>