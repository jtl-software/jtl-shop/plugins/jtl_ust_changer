<style>
    #loader .modal.modal-center {
        text-align: center;
        padding: 0!important;
    }

    #loader .modal.modal-center:before {
        content: '';
        display: inline-block;
        height: 100%;
        vertical-align: middle;
        margin-right: -4px;
    }

    #loader .modal.modal-center .modal-dialog {
        display: inline-block;
        text-align: left;
        vertical-align: middle;
    }
    #loader #loading-body {
        margin: 20px;
        text-align: center;;
    }
    .card-bg {
        background-color: #eee;
    }
    .hr-color {
        border-top-color: #b0b0b0;
    }
    .update-btn {
        column-span: all;
        padding: 10px 25px;
    }
    .update-form {
        column-count: 1!important;
    }
</style>
{$alertList->displayAlertByKey('isStuck')}
<div class="info-text">
    <p>
    <div class="alert alert-warning">
        <strong>{__('WICHTIG')}:</strong> {__('Dieses Plugin dient nur als Hilfestellung für JTL-Shops, deren Abgleich mit JTL-Wawi sehr lange dauert! Es ersetzt nicht die Maßnahmen in JTL-Wawi! Wenn der Abgleich mit allen Artikeln (keine Bilder, Kategorien etc.) in akzeptabler Zeit fertiggestellt wird, empfehlen wir folgende Schritte: Führen Sie nur die JTL-Wawi-Anpassungen durch, setzen Sie unter Onlineshop > Komplettabgleich den Haken bei "Globale Daten" und "Artikel" und gleichen Sie bis zur Fertigstellung bei aktivem Wartungsmodus ab.')}
    </div>
    <br>
    {__('Bitte beachten Sie auch die jeweils aktuellen Informationen sowie die verschiedenen Methoden/Vorschläge zur besten Vorgehensweise im')} <a href="https://forum.jtl-software.de/threads/mehrwertsteuer-senkung-vom-01-07-31-12-2020-offizieller-diskussionthread-video.129542/" target="_blank"><strong>{__('Forenthread zum Thema')}</strong></a>.<br>
    <br>
    {__('Sie können dieses Plugin nutzen, wenn das Standardvorgehen bei Ihnen zu einer zu langen Downtime führen würde.')}<br>
    <div class="alert" style="background-color: #eee;">
        {__('Geeignet für: JTL-Shops (nur Version 5.0 und PHP-Version mindestens 7.3), deren Abgleich aller Artikel (keine Kategorien, Bilder etc.) eine zu lange Downtime bedeuten würde.')}<br>
        <br>
        <strong>{__('Hinweis')}:</strong> {__('Falls gewünscht können Sie bei Nutzung des Plugins die Bruttopreise beibehalten. Allerdings erhalten alle Kundengruppen, bei denen in JTL-Wawi die Nettopreisanzeige aktiviert sind, über das Plugin keine angepassten Nettopreise!')}<br>
        <br>
        <ul>
            <li>1. {__('Vorbereitung: Laden Sie das Plugin herunter und installieren Sie es in JTL-Shop, führen Sie aber noch nichts durch.')}</li>
            <li>2. {__('Stoppen Sie den Abgleich.')}</li>
            <li>3. {__('Passen Sie Steuersätze in JTL-Wawi an (und ggf. die Artikelpreise, wenn Sie die Bruttopreise behalten wollen; QuickSync bei der Ameise nutzen!).')}</li>
            <li>4. {__('Aktivieren Sie den Wartungsmodus in JTL-Shop spätestens jetzt bzw. zur Stichzeit.')}</li>
            <li>5. {__('Führen Sie zur Sicherheit eine Datenbanksicherung von JTL-Shop durch (siehe auch unseren')} <a href="https://guide.jtl-software.de/jtl-shop-4/wartung/datenbank-backups-von-jtl-shop-erstellen/" target="_blank">{__('Guidebeitrag')}</a> {__('dazu. Hostingkunden bei JTL können sich hier an Methode 2 „Möglichkeit 2: Backup via Plesk“ halten).')}</li>
            <li>6. {__('Führen Sie im Plugin die entsprechenden Änderungen für alle betroffenen Steuersätze durch: Tragen Sie dazu nacheinander für alle genutzten und betroffenen Steuersätze bei "Alte USt." die alte und bei "Neue USt." die neue Ust. ohne %-Zeichen ein. Verwenden Sie die Option "Bruttopreise behalten" nur, wenn auch die Preise in JTL-Wawi entsprechend angepasst wurden! Beachten Sie den Hinweis bzgl. der Nettopreisanzeige! Klicken Sie anschließend auf "Update durchführen". Diese Schaltfläche kann nur gewählt werden, wenn der Wartungsmodus aktiv ist.')}</li>
            <li>7. {__('Starten Sie den Abgleich. Wenn Sie keine Preise in JTL-Wawi angepasst haben, sollte der Abgleich nur wenige Sekunden dauern.')}</li>
            <li>8. {__('Prüfen Sie die Artikel und leeren Sie ggf. den Artikel-Cache.')}</li>
            <li>9. {__('Beenden Sie den Wartungsmodus.')}</li>
            <li>10. {__('Falls Sie in JTL-Wawi Preise angepasst hatten, lassen Sie den Abgleich in Ruhe durchlaufen, während der Shop schon fertig ist.')}</li>
        </ul>
    </div>
    </p>
</div>
<br>

<h3 class="panel-title">{__('Steuereinstellungen')}</h3>
<div id="settings">
    <form class="navbar-form settings update-form"  id="form" method="post" action="{$postUrl}">
        {$jtl_token}
        <input type="hidden" name="update" value="1">
        {if $stuck === true}
            <input type="hidden" name="stuck" value="1">
        {/if}
        <div class="card card-bg">
            <div class="card-header">
                <span class="subheading1">{__('Tragen Sie den aktuellen Steuersatz als Zahl ein (ohne %-Zeichen, z.B. 19)')}</span>
                <hr class="mb-n3 hr-color" />
            </div>
            <div class="card-body">
                <div class="form-group form-row align-items-center">
                    <label class="col col-sm-4 col-form-label text-sm-right order-1" for="oldVAT">{__('Alte Ust.')}</label>
                    <div class="col-sm pl-sm-3 pr-sm-5 order-last order-sm-2">
                        <input class="form-control" type="text" name="oldVAT" id="oldVAT" value="{$config->getUstValue()->getOldVAT()}" {if $stuck === true}disabled{/if}>
                    </div>
                </div>
            </div>
        </div>
        <div class="card card-bg">
            <div class="card-header">
                <span class="subheading1">{__('Tragen Sie den Steuersatz als Zahl ein, den die Artikel anstelle des alten Steuersatzes erhalten sollen (ohne %-Zeichen, z. B. 16 oder 5)')}</span>
                <hr class="mb-n3 hr-color" />
            </div>
            <div class="card-body">
                <div class="form-group form-row align-items-center">
                    <label class="col col-sm-4 col-form-label text-sm-right order-1" for="newVAT">{__('Neue Ust.')}</label>
                    <div class="col-sm pl-sm-3 pr-sm-5 order-last order-sm-2">
                        <input class="form-control" type="text" name="newVAT" id="newVAT" value="{$config->getUstValue()->getNewVAT()}" {if $stuck === true}disabled{/if}>
                    </div>
                </div>
            </div>
        </div>
        <div class="card card-bg">
            <div class="card-header">
                <span class="subheading1">{__('Wählen Sie aus, ob Ihre Bruttopreise gleichbleiben sollen (Nettopreise neu berechnen oder nicht)')}</span>
                <hr class="mb-n3 hr-color" />
            </div>
            <div class="card-body">
                <div class="form-group form-row align-items-center">
                    <label class="col col-sm-4 col-form-label text-sm-right order-1"  for="keepGrossPrice">{__('Bruttopreise für Verbraucher-Kundengruppen beibehalten?')}</label>
                    <div class="col-sm pl-sm-3 pr-sm-5 order-last order-sm-2">
                        <select class="form-control" name="keepGrossPrice" id="keepGrossPrice"  {if $stuck === true}disabled{/if}>
                            <option value="Y" {if $config->isKeepGrossPrice() === true}selected{/if}>{__('Ja')}</option>
                            <option value="N" {if $config->isKeepGrossPrice() === false}selected{/if}>{__('Nein')}</option>
                        </select>
                    </div>
                    <div class="col-auto ml-sm-n4 order-2 order-sm-3">
                        <span data-html="true" data-toggle="tooltip" data-placement="left" title='{__('Wenn Sie \"Ja\" wählen, werden die Nettopreise neu berechnet, sodass die Bruttopreise gleichbleiben. Dies gilt <strong>nicht</strong> für Kundengruppen, denen Nettopreise angezeigt werden.')}'>
                            <span class="fas fa-info-circle fa-fw"></span>
                        </span>
                    </div>
                </div>
            </div>
        </div>
        <div class="update-btn">
            <div class="row">
                <div class="alert alert-warning text-left">
                    <strong>{__('WICHTIG')}:</strong> {__('Merken Sie sich genau, welche Änderung Sie durchgeführt haben. Nur mit exakt umgekehrten Einstellungen (außer für die Bruttopreis-Anpassung) können Sie die Änderungen wieder zurückdrehen! Machen Sie ggf. eine Datenbanksicherung, bevor Sie auf "Update durchführen" klicken.')}
                </div>
                <div class="float-left">
                    <button type="submit" class="btn btn-danger" {if $maintenance === false}disabled{/if}>{__('Update durchführen')}</button>
                </div>
            </div>
        </div>
    </form>
</div>
<br>
<div class="info">
    <p>
        {__('Führen Sie das Update für alle Ihre betroffenen Steuersätze separat durch (wenn Sie z. B. Artikel mit 19 % und 7 % USt. im Sortiment haben).')}<br>
        <br>
        <strong>{__('WICHTIG')} {__('BEVOR SIE DEN WARTUNGSMODUS WIEDER DEAKTIVIEREN')}:</strong><br>
        <br>
    <ul>
        <li>1. {__('Wenn Sie in JTL-Wawi die Preise angepasst haben und entsprechend zuvor "Bruttopreise beibehalten=Ja" gewählt haben, setzen Sie in JTL-Wawi unter Onlineshop > Komplettabgleich die Haken bei "Globale Daten" und "Artikel". Wenn Sie die Vergünstigung an Ihre Endkunden weitergeben wollen, ist dies nicht notwendig! Starten Sie den JTL-Shop-Abgleich.')}</li>
        <li>2. {__('Löschen Sie den Artikel-Cache unter')} <a href="{$backendPath}cache.php" target="_blank">{__('Administration')}->{__('System')}->{__('Cache')}</a>.<br>
            <img class="img-responsive" style="max-width: 700px;"src="{$config->plugin->getPaths()->getAdminURL()}template/cache.gif" alt="cache-image">
        </li>
        <li>3. {__('Prüfen Sie Ihre Artikel')}</li>
    </ul>
    </p>
</div>

<div id="loader">
    <div class="modal modal-center fade" id="loading-modal" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <h2 id="loading-body"><i class="fa fa-spinner fa-spin fa-fw"></i> {__('Update wird durchgeführt. Bitte warten...')}</h2>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(function() {
        $('#form').submit(function() {

            $(this).find('input[type="submit"]')
                .addClass('disabled')
                .attr('disabled', true);

            $('#loading-modal').modal({
                backdrop: 'static'
            });

            $('#loading-modal').modal('show');
        });
    });
</script>
