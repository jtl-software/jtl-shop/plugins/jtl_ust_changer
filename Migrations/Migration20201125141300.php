<?php declare(strict_types=1);

namespace Plugin\jtl_ust_changer\Migrations;

use Illuminate\Support\Collection;
use JTL\DB\ReturnType;
use JTL\Plugin\Migration;
use JTL\Update\DBMigrationHelper;
use JTL\Update\IMigration;

class Migration20201125141300 extends Migration implements IMigration
{
    /**
     * @inheritDoc
     */
    public function up()
    {
        //Drop deprecated plugin-table tpreise:
        $this->execute('DROP TABLE IF EXISTS xplugin_jtl_ust_changer_tpreise');
        
        //Match already existing plugin-tables:
        $existingPluginTables = [
            'xplugin_jtl_ust_changer_einstellungen' => false,
            'xplugin_jtl_ust_changer_history'       => false,
            'xplugin_jtl_ust_changer_tpreisdetail'  => false,
            'xplugin_jtl_ust_changer_tsonderpreise' => false
        ];
        /** @var Collection $existingPluginTablesFromDB */
        $existingPluginTablesFromDB = $this->db->queryPrepared(
            'SELECT TABLE_NAME FROM information_schema.TABLES
                WHERE TABLE_SCHEMA = :db AND TABLE_NAME LIKE :table',
            [
                'db'    => $this->db->getConfig()['database'],
                'table' => 'xplugin_jtl_ust_changer_%'
            ],
            ReturnType::COLLECTION
        );

        foreach (\array_keys($existingPluginTables) as $tableName) {
            $existingPluginTables[$tableName] = $existingPluginTablesFromDB->contains('TABLE_NAME', $tableName);
            //If Migration to utf8 is needed, migrate table to utf8
            if ($existingPluginTables[$tableName] === true && DBMigrationHelper::isTableNeedMigration($tableName)) {
                DBMigrationHelper::migrateToInnoDButf8($tableName);
            }
        }

        if ($existingPluginTables['xplugin_jtl_ust_changer_einstellungen'] === false) {
            $this->execute(
                'CREATE TABLE IF NOT EXISTS xplugin_jtl_ust_changer_einstellungen(
                    id INT NOT NULL AUTO_INCREMENT,
                    name VARCHAR(255) NOT NULL,
                    wert VARCHAR(255) NOT NULL,
                    PRIMARY KEY(id),
                    UNIQUE KEY xplugin_jtl_ust_changer_einstellungen_name_uindex(name)
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci'
            );
            $this->execute(
                "INSERT INTO xplugin_jtl_ust_changer_einstellungen(name, wert)
                    VALUES('keepGrossPrice', 'N'),
                        ('newVAT', '19'),
                        ('oldVAT', '16');"
            );
        } else {
            $this->execute(
                "UPDATE xplugin_jtl_ust_changer_einstellungen SET wert = '19' WHERE name = 'newVAT'"
            );
            $this->execute(
                "UPDATE xplugin_jtl_ust_changer_einstellungen SET wert = '16' WHERE name = 'oldVAT'"
            );
        }

        if ($existingPluginTables['xplugin_jtl_ust_changer_history'] === false) {
            $this->execute(
                'CREATE TABLE IF NOT EXISTS xplugin_jtl_ust_changer_history(
                    historyId INT NOT NULL AUTO_INCREMENT,
                    oldVAT FLOAT NOT NULL,
                    newVAT FLOAT NOT NULL,
                    startTime DATETIME NOT NULL,
                    endTime DATETIME DEFAULT NULL NULL,
                    keepGrossPrice BOOL DEFAULT FALSE NOT NULL,
                    PRIMARY KEY(historyId)
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci'
            );
        }

        if ($existingPluginTables['xplugin_jtl_ust_changer_tpreisdetail'] === false) {
            $this->execute('CREATE TABLE IF NOT EXISTS xplugin_jtl_ust_changer_tpreisdetail LIKE tpreisdetail');
            $this->execute(
                'ALTER TABLE xplugin_jtl_ust_changer_tpreisdetail
                    DROP PRIMARY KEY,
                    DROP KEY kPreis_nAnzahlAb,
                    MODIFY kPreisDetail INT NOT NULL,
                    ADD modified DATETIME DEFAULT NULL,
                    ADD historyId INT NOT NULL,
                    ADD UNIQUE(historyId, kPreisDetail)'
            );
        }

        if ($existingPluginTables['xplugin_jtl_ust_changer_tsonderpreise'] === false) {
            $this->execute('CREATE TABLE IF NOT EXISTS xplugin_jtl_ust_changer_tsonderpreise LIKE tsonderpreise');
            $this->execute(
                'ALTER TABLE xplugin_jtl_ust_changer_tsonderpreise
                    DROP PRIMARY KEY,
                    ADD modified DATETIME DEFAULT NULL,
                    ADD historyId INT NOT NULL,
                    ADD UNIQUE(historyId, kArtikelSonderpreis, kKundengruppe)'
            );
        }
    }

    /**
     * @inheritDoc
     */
    public function down()
    {
        if ($this->doDeleteData()) {
            $this->execute('DROP TABLE IF EXISTS xplugin_jtl_ust_changer_einstellungen');
            $this->execute('DROP TABLE IF EXISTS xplugin_jtl_ust_changer_history');
            $this->execute('DROP TABLE IF EXISTS xplugin_jtl_ust_changer_tpreisdetail');
            $this->execute('DROP TABLE IF EXISTS xplugin_jtl_ust_changer_tsonderpreise');
        }
    }
}
