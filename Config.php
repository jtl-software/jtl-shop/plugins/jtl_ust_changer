<?php declare(strict_types=1);

namespace Plugin\jtl_ust_changer;

use Exception;
use JTL\DB\DbInterface;
use JTL\DB\ReturnType;
use JTL\Plugin\Plugin;
use JTL\Shop;

/**
 * Class Config
 * @package Plugin\jtl_ust_changer
 */
class Config
{
    /** @var string  */
    const PLUGIN_ID = 'jtl_ust_changer';

    /** @var  DbInterface */
    public $db;

    /** @var Plugin */
    public $plugin;

    /** @var bool  */
    private $keepGrossPrice;

    /** @var UStValue */
    private $ustValue;

    /**
     * Config constructor.
     * @param Plugin      $plugin
     * @param DbInterface $db
     */
    public function __construct(Plugin $plugin, DbInterface $db)
    {
        $this->db     = $db;
        $this->plugin = $plugin;
        $this->getSettingsFromDB();
    }

    /**
     * get Settings from DB.
     * @return void
     */
    private function getSettingsFromDB(): void
    {
        $settings       = $this->db->queryPrepared(
            'SELECT `name`, `wert` FROM xplugin_jtl_ust_changer_einstellungen',
            [],
            ReturnType::ARRAY_OF_OBJECTS
        );
        $this->ustValue = new UStValue();
        foreach ($settings as $setting) {
            switch ($setting->name) {
                case 'keepGrossPrice':
                    $this->setKeepGrossPrice($setting->wert === 'Y');
                    break;
                case 'oldVAT':
                    $this->ustValue->setOldVAT((float)$setting->wert);
                    break;
                case 'newVAT':
                    $this->ustValue->setNewVAT((float)$setting->wert);
                    break;
            }
        }
    }

    /**
     * Update the settings in the Database.
     * @return $this
     */
    public function updateConfig(): Config
    {
        $this->db->queryPrepared(
            "UPDATE xplugin_jtl_ust_changer_einstellungen SET wert = :keepGrossPrice WHERE name = 'keepGrossPrice'",
            ['keepGrossPrice' => $this->keepGrossPrice === true ? 'Y' : 'N'],
            ReturnType::AFFECTED_ROWS
        );
        $this->db->queryPrepared(
            "UPDATE xplugin_jtl_ust_changer_einstellungen SET wert = :oldVAT WHERE name = 'oldVAT'",
            ['oldVAT' => $this->ustValue->getOldVAT()],
            ReturnType::AFFECTED_ROWS
        );
        $this->db->queryPrepared(
            "UPDATE xplugin_jtl_ust_changer_einstellungen SET wert = :newVAT WHERE name = 'newVAT'",
            ['newVAT' => $this->ustValue->getNewVAT()],
            ReturnType::AFFECTED_ROWS
        );

        return $this;
    }

    /**
     * get the Plugin-id (kPlugin).
     *
     * @return int
     */
    public function getkPlugin(): int
    {
        return $this->plugin->getID();
    }

    /**
     * get the Plugin-Id
     *
     * @return string
     */
    public function getPluginId(): string
    {
        return $this->plugin->getPluginID();
    }

    /**
     * Gets the Tab-Url for cPluginTab
     *
     * @param string $tabname
     *
     * @return string
     */
    public function getTabUrl(string $tabname): string
    {
        return $this->getPluginBackendUrl() . '&cPluginTab=' . $tabname;
    }

    /**
     * Gets the Plugin-URL itself like shopdomain.tld/admin/plugin.php?kPlugin=XX
     *
     * @return string
     */
    public function getPluginBackendUrl(): string
    {
        return 'plugin.php?kPlugin=' . $this->getkPlugin();
    }

    /**
     * @return bool
     */
    public function isKeepGrossPrice(): bool
    {
        return $this->keepGrossPrice;
    }

    /**
     * @param bool $keepGrossPrice
     * @return Config
     */
    public function setKeepGrossPrice(bool $keepGrossPrice): Config
    {
        $this->keepGrossPrice = $keepGrossPrice;

        return $this;
    }

    /**
     * @return UStValue
     */
    public function getUstValue(): UStValue
    {
        return $this->ustValue;
    }

    /**
     * @param UStValue $ustValue
     * @return Config
     */
    public function setUstValue(UStValue $ustValue): Config
    {
        $this->ustValue = $ustValue;

        return $this;
    }

    /**
     * get the current Process-Config from Database.
     *
     * @return array
     * @throws Exception
     */
    public function getCurrentProcessFromDB(): array
    {
        $query = $this->db->queryPrepared(
            'SELECT historyId, newVAT, oldVAT, startTime, keepGrossPrice FROM xplugin_jtl_ust_changer_history 
                    WHERE endTime IS NULL ORDER BY startTime',
            [],
            ReturnType::ARRAY_OF_ASSOC_ARRAYS
        );

        if (\count($query) > 1) {
            $message = __('Es darf nur einen unerledigten Vorgang geben!');

            throw new Exception($message);
        }

        return \count($query) === 1 ? $query[0] : [];
    }

    /**
     * Checks if the Maintenancemode is on.
     *
     * @return bool
     */
    public function isMaintenanceOn(): bool
    {
        return Shop::getSettingValue(1, 'wartungsmodus_aktiviert') === 'Y';
    }
}
