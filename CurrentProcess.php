<?php declare(strict_types=1);

namespace Plugin\jtl_ust_changer;

/**
 * Class CurrentProcess
 * @package Plugin\jtl_ust_changer
 */
class CurrentProcess
{
    /** @var int - From Database: xplugin_jtl_ust_changer_history.historyId */
    private $historyId;

    /** @var string - From Database: xplugin_jtl_ust_changer_history.startTime */
    private $startTime;

    /** @var UStValue */
    private $ustValue;

    /** @var bool */
    private $keepGrossPrice;

    /**
     * CurrentProcess constructor.
     * @param UStValue $ustValue
     * @param int      $historyId
     * @param string   $startTime
     */
    public function __construct(
        UStValue $ustValue,
        int $historyId = 0,
        string $startTime = ''
    ) {
        $this->ustValue  = $ustValue;
        $this->historyId = $historyId;
        $this->startTime = $startTime;
    }

    /**
     * @return int
     */
    public function getHistoryId(): int
    {
        return $this->historyId;
    }

    /**
     * @param mixed $historyId
     * @return $this
     */
    public function setHistoryId($historyId): self
    {
        $this->historyId = $historyId;

        return $this;
    }

    /**
     * @return string
     */
    public function getStartTime(): string
    {
        return $this->startTime;
    }

    /**
     * @param string $startTime
     *
     * @return CurrentProcess
     */
    public function setStartTime(string $startTime): CurrentProcess
    {
        $this->startTime = $startTime;

        return $this;
    }

    /**
     * @return UStValue
     */
    public function getUstValue(): UStValue
    {
        return $this->ustValue;
    }

    /**
     * @param UStValue $ustValue
     *
     * @return $this
     */
    public function setUstValue(UStValue $ustValue): self
    {
        $this->ustValue = $ustValue;

        return $this;
    }

    /**
     * @return bool
     */
    public function isKeepGrossPrice(): bool
    {
        return $this->keepGrossPrice;
    }

    /**
     * @param bool $keepGrossPrice
     * @return $this
     */
    public function setKeepGrossPrice(bool $keepGrossPrice): self
    {
        $this->keepGrossPrice = $keepGrossPrice;

        return $this;
    }
}
