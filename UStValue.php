<?php declare(strict_types=1);

namespace Plugin\jtl_ust_changer;

/**
 * Class UStValue
 * @package Plugin\jtl_ust_changer
 */
class UStValue
{
    /** @var float  */
    private $oldVAT;

    /** @var float  */
    private $newVAT;

    /**
     * UStValue constructor.
     *
     * @param float $oldVAT
     * @param float $newVAT
     */
    public function __construct(float $oldVAT = 19.00, float $newVAT = 16.00)
    {
        $this->setOldVAT($oldVAT);
        $this->setNewVAT($newVAT);
    }

    /**
     * @return float
     */
    public function getOldVAT(): float
    {
        return $this->oldVAT;
    }

    /**
     * @param float $oldVAT
     * @return $this
     */
    public function setOldVAT(float $oldVAT): self
    {
        $this->oldVAT = $oldVAT;

        return $this;
    }

    /**
     * @return float
     */
    public function getNewVAT(): float
    {
        return $this->newVAT;
    }

    /**
     * @param float $newVAT
     * @return $this
     */
    public function setNewVAT(float $newVAT): self
    {
        $this->newVAT = $newVAT;

        return $this;
    }
}
