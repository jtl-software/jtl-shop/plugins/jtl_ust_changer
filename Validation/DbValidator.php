<?php declare(strict_types=1);

namespace Plugin\jtl_ust_changer\Validation;

use Exception;
use InvalidArgumentException;

class DbValidator extends Validator
{
    /**
     * @param mixed $historyId
     *
     * @return int|null
     */
    public function validateHistoryId($historyId): ?int
    {
        if (\is_numeric($historyId)) {
            $historyIdInt = (int)$historyId;
            if ((float)$historyId == $historyIdInt && $historyIdInt > 0) {
                return $historyIdInt;
            }
        }

        return null;
    }

    /**
     * @param mixed $time
     *
     * @return string|null
     */
    public function validateTimeString($time): ?string
    {
        if (\is_string($time)) {
            $timestamp = \strtotime($time);
            if ($timestamp === false) {
                return null;
            }
            try {
                $dateTimeObj = (new \DateTime())->setTimestamp($timestamp);
            } catch (Exception $e) {
                return null;
            }
            if ($dateTimeObj->format('Y-m-d H:i:s') === $time) {
                return $time;
            }
        }

        return null;
    }

    /**
     * validate the output from Config()->getCurrentProcessFromDB()'
     *
     * @param array<string, string> $currentProcess
     *
     * @return array<string, mixed>
     * @throws InvalidArgumentException
     */
    public function validateCurrentProcess(array $currentProcess): array
    {
        $historyId = $this->validateHistoryId($currentProcess['historyId'] ?? null);
        if ($historyId === null) {
            $message = __('historyID muss eine Zahl und >= 0 sein.');

            throw new InvalidArgumentException($message);
        }

        $oldVAT = $this->validateVAT($currentProcess['oldVAT'] ?? null);
        $newVAT = $this->validateVAT($currentProcess['newVAT'] ?? null);
        if ($oldVAT === null || $newVAT === null) {
            $message = __('Der Betrag der Ust. muss größer als 0 sein.') . ' ' .
                \sprintf(__('(alt, neu): %s, %s'), $oldVAT, $newVAT);

            throw new InvalidArgumentException($message);
        }

        $startTime = $this->validateTimeString($currentProcess['startTime'] ?? null);
        if ($startTime === null) {
            $message = __("startTime ist kein String des Formats: 'Y-m-d H:i:s'");

            throw new InvalidArgumentException($message);
        }

        $keepGrossPrice = $this->validateKeepGrossPrice($currentProcess['keepGrossPrice'] ?? null);
        if ($keepGrossPrice === null) {
            $message = \sprintf(__('keepGrossPrice darf nur folgende Werte haben: %s'), '"Y", "N"');

            throw new InvalidArgumentException($message);
        }

        return [
            'historyId'      => $historyId,
            'newVAT'         => $newVAT,
            'oldVAT'         => $oldVAT,
            'startTime'      => $startTime,
            'keepGrossPrice' => $keepGrossPrice
        ];
    }
}
