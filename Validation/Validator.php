<?php declare(strict_types=1);

namespace Plugin\jtl_ust_changer\Validation;

class Validator
{
    /**
     * @param mixed $vat
     *
     * @return float|null
     */
    public function validateVAT($vat): ?float
    {
        if (\is_numeric($vat)) {
            $vat = (float)$vat;
            if ($vat > 0.00) {
                return $vat;
            }
        }

        return null;
    }

    /**
     * @param mixed $keepGrossPrice
     *
     * @return bool|null
     */
    public function validateKeepGrossPrice($keepGrossPrice): ?bool
    {
        if (\is_bool($keepGrossPrice)) {
            return $keepGrossPrice;
        }
        if (\in_array($keepGrossPrice, ['Y', 'N', '1', '0'])) {
            return \in_array($keepGrossPrice, ['Y', '1']);
        }

        return null;
    }
}
