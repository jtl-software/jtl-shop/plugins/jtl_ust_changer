<?php declare(strict_types=1);

namespace Plugin\jtl_ust_changer;

use InvalidArgumentException;

/**
 * Class CurrentProcessVAT
 * @package Plugin\jtl_ust_changer
 */
class CurrentProcessVAT extends UStValue
{
    /** @var float */
    private $oldVAT2;

    /** @var float */
    private $newVAT2;

    /**
     * CurrentProcessVAT constructor.
     *
     * @param float $oldVAT
     * @param float $newVAT
     * @throws InvalidArgumentException
     */
    public function __construct(float $oldVAT = 19.00, float $newVAT = 16.00)
    {
        parent::__construct($oldVAT, $newVAT);
        $this->oldVAT2 = 1.00 + $this->getOldVAT() / 100;
        $this->newVAT2 = 1.00 + $this->getNewVAT() / 100;
    }

    /**
     * @return float
     */
    public function getOldVAT2(): float
    {
        return $this->oldVAT2;
    }

    /**
     * @return float
     */
    public function getNewVAT2(): float
    {
        return $this->newVAT2;
    }
}
