<?php declare(strict_types=1);

namespace Plugin\jtl_ust_changer\BackendTab;

use JTL\DB\DbInterface;
use JTL\Plugin\Plugin;
use JTL\Services\DefaultServicesInterface;
use JTL\Services\JTL\AlertServiceInterface;
use JTL\Shop;
use Plugin\jtl_ust_changer\Config;
use JTL\Smarty\JTLSmarty;
use SmartyException;

/**
 * Class BackendTab
 * @package Plugin\jtl_ust_changer\BackendTab
 */
abstract class BackendTab
{
    /** @var JTLSmarty */
    protected $smarty;

    /** @var Plugin */
    protected $plugin;

    /** @var DbInterface */
    protected $db;

    /** @var Config */
    protected $config;

    /** @var DefaultServicesInterface */
    protected $container;

    /** @var AlertServiceInterface */
    protected $alertService;

    /**
     * BackendTab constructor.
     *
     * @param JTLSmarty $smarty
     *
     */
    public function __construct(JTLSmarty $smarty)
    {
        $this->container    = Shop::Container();
        $this->smarty       = $smarty;
        $this->config       = $this->container->get(Config::class);
        $this->plugin       = $this->config->plugin;
        $this->db           = $this->container->getDB();
        $this->alertService = $this->container->getAlertService();

        if (!empty($_POST)) {
            $this->onPost();
        }
    }

    /**
     * will be used if there is any post-event to handle.
     */
    protected function onPost(): void
    {
    }

    /**
     * method for rendering a template.
     *
     * @return string
     *
     * @throws SmartyException
     */
    abstract public function render(): string;
}
