<?php declare(strict_types=1);

namespace Plugin\jtl_ust_changer\BackendTab;

use Exception;
use JTL\Alert\Alert;
use JTL\Shop;
use Plugin\jtl_ust_changer\ChangeVAT;
use Plugin\jtl_ust_changer\Validation\Validator;
use SmartyException;

/**
 * Class Update
 * @package Plugin\jtl_ust_changer\BackendTab
 */
final class Update extends BackendTab
{
    /** @var string  */
    public const TAB_NAME = 'Update';

    /**
     * @inheritDoc
     */
    public function render(): string
    {
        $maintenance = $this->config->isMaintenanceOn();
        if ($maintenance === false && !$this->alertService->getAlert('UpdateException')) {
            $this->alertService->addAlert(
                Alert::TYPE_ERROR,
                __('Wartungsmodus ist ausgeschaltet!'),
                'maintenanceOff',
                [
                    'dismissable' => false,
                    'fadeOut' => Alert::FADE_NEVER
                ]
            );
            return $this->getRenderReturn(false, false);
        }
        try {
            $history = count($this->config->getCurrentProcessFromDB()) > 0;
        } catch (Exception $e) {
            $this->alertService->addAlert(
                Alert::TYPE_ERROR,
                '<strong>' . \get_class($e) . ': ' . $e->getMessage() . '</strong>',
                'moreThanOneUnfinishedProcess'
            );
            return $this->getRenderReturn(false, true);
        }

        // if there is an unfinished process, display Alert and render template with $stuck = true.
        if ($history) {
            $this->alertService->addAlert(
                Alert::TYPE_ERROR,
                __('Ein Vorgang wurde nicht erfolgreich beendet. Bitte führen Sie das Update erneut aus.'),
                'isStuck',
                [
                    'dismissable'             => false,
                    'fadeOut'                 => Alert::FADE_NEVER,
                    'showInAlertListTemplate' => false
                ]
            );

            return $this->getRenderReturn(true, true);
        }

        return $this->getRenderReturn(false, true);
    }

    /**
     * @param bool $stuck
     * @param bool $maintenance
     *
     * @return string
     * @throws SmartyException
     */
    private function getRenderReturn(bool $stuck, bool $maintenance): string
    {
        return $this->smarty
            ->assignByRef('config', $this->config)
            ->assign('maintenance', $maintenance)
            ->assign('backendPath', Shop::getAdminURL() . '/')
            ->assign('postUrl', Shop::getAdminURL() . '/' . $this->config->getTabUrl(self::TAB_NAME))
            ->assign('stuck', $stuck)
            ->fetch($this->plugin->getPaths()->getAdminPath() . 'template/update.tpl');
    }

    /**
     * @inheritDoc
     */
    protected function onPost(): void
    {
        if (!isset($_POST['update'])) {
            return;
        }

        if (!isset($_POST['stuck'])) {
            // validate POST-Input:
            $postValues = $this->validatePostValues($_POST);
            if ($postValues === null) {
                return;
            }
            $oldVAT         = $postValues['oldVAT'];
            $newVAT         = $postValues['newVAT'];
            $keepGrossPrice = $postValues['keepGrossPrice'];


            $ustValue = $this->config->getUstValue();
            $ustValue->setOldVAT($oldVAT);
            $ustValue->setNewVAT($newVAT);
            $this->config->setKeepGrossPrice($keepGrossPrice);
            $this->config->setUstValue($ustValue);
            $this->config->updateConfig();
        }

        try {
            $changeVAT = new ChangeVAT();
            $changeVAT->updateDB();
        } catch (Exception $e) {
            $this->alertService->addAlert(
                Alert::TYPE_ERROR,
                $e->getMessage(),
                'UpdateException'
            );
            return;
        }
        $this->alertService->addAlert(
            Alert::TYPE_SUCCESS,
            __('Update wurde erfolgreich durchgeführt.'),
            'UpdateSetOldVATInvalidArgument',
            ['dismissable' => true]
        );
    }

    /**
     * validates the given POST-Values, return false on error.
     *
     * @param array<string, string> $postVar
     *
     * @return array<string, mixed>|null
     */
    private function validatePostValues(array $postVar): ?array
    {
        $validator          = $this->container->get(Validator::class);
        $notNumeric         = __('Es wurde keine gültige Zahl für die %s Ust. angegeben.');
        $wrongNumericFormat = __('Der Betrag der Ust. muss größer als 0 sein.');
        $wrongBoolFormat    = __('keepGrossPrice darf nur folgende Werte haben: %s');

        // check oldVAT:
        $oldVAT = $validator->validateVAT($postVar['oldVAT'] ?? null);
        if ($oldVAT === null) {
            $this->alertService->addAlert(
                Alert::TYPE_ERROR,
                \sprintf($notNumeric, __('alte')) . ' ' .$wrongNumericFormat,
                'UpdateSetOldVATInvalidArgument'
            );
            return null;
        }

        // check newVAT:
        $newVAT = $validator->validateVAT($postVar['newVAT'] ?? null);
        if ($newVAT === null) {
            $this->alertService->addAlert(
                Alert::TYPE_ERROR,
                \sprintf($notNumeric, __('neue')) . ' ' . $wrongNumericFormat,
                'UpdateSetNewVATInvalidArgument'
            );
            return null;
        }

        // check keepGrossPrice:
        $keepGrossPrice = $validator->validateKeepGrossPrice($postVar['keepGrossPrice'] ?? null);
        if ($keepGrossPrice === null) {
            $this->alertService->addAlert(
                Alert::TYPE_ERROR,
                \sprintf($wrongBoolFormat, '"Y", "N'),
                'UpdateSetkeepGroosPriceInvalidArgument'
            );
            return null;
        }

        return ['oldVAT' => $oldVAT, 'newVAT' => $newVAT, 'keepGrossPrice' => $keepGrossPrice];
    }
}
