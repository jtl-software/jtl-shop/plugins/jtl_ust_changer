<?php declare(strict_types=1);

namespace Plugin\jtl_ust_changer\BackendTab;

use JTL\Alert\Alert;

/**
 * Class History
 * @package Plugin\jtl_ust_changer\BackendTab
 */
final class History extends BackendTab
{
    /** @var string  */
    public const TAB_NAME = 'History';

    /**
     * @inheritDoc
     */
    public function render(): string
    {
        $history = $this->db->selectAll(
            'xplugin_jtl_ust_changer_history',
            [],
            [],
            'historyId, oldVAT, newVAT, startTime, endTime, keepGrossPrice',
            'startTime'
        );

        if (!$history) {
            $this->alertService->addAlert(
                Alert::TYPE_INFO,
                __('Keine Daten vorhanden.'),
                'noHistoryData',
                [
                    'dismissable'             => false,
                    'fadeOut'                 => Alert::FADE_NEVER,
                    'showInAlertListTemplate' => false
                ]
            );
        }

        return $this->smarty
            ->assign('history', $history)
            ->fetch($this->plugin->getPaths()->getAdminPath() . 'template/history.tpl');
    }
}
