<?php declare(strict_types=1);

namespace Plugin\jtl_ust_changer;

use Exception;
use JTL\Events\Dispatcher;
use JTL\Plugin\Bootstrapper;
use JTL\Plugin\Plugin;
use JTL\Shop;
use JTL\Smarty\JTLSmarty;
use Plugin\jtl_ust_changer\BackendTab\History;
use Plugin\jtl_ust_changer\BackendTab\Update;
use Plugin\jtl_ust_changer\Validation\DbValidator;
use Plugin\jtl_ust_changer\Validation\Validator;

/**
 * Class Bootstrap
 * @package Plugin\jtl_ust_changer
 */
class Bootstrap extends Bootstrapper
{
    /**
     * @inheritDoc
     */
    public function boot(Dispatcher $dispatcher)
    {
        parent::boot($dispatcher);

        /** @var Plugin $plugin */
        $plugin = $this->getPlugin();

        Shop::Container()->setSingleton(Config::class, function () use ($plugin) {
            return new Config($plugin, $this->getDB());
        });
        Shop::Container()->setSingleton(Validator::class, function () {
            return new Validator();
        });
        Shop::Container()->setSingleton(DbValidator::class, function () {
            return new DbValidator();
        });
    }

    /**
     * @inheritDoc
     */
    public function renderAdminMenuTab(string $tabName, int $menuID, JTLSmarty $smarty): string
    {
        switch ($tabName) {
            case 'Update':
                return (new Update($smarty))->render();
            case 'History':
                return (new History($smarty))->render();
            default:
                return '';
        }
    }
}
