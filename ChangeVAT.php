<?php declare(strict_types=1);

namespace Plugin\jtl_ust_changer;

use Exception;
use InvalidArgumentException;
use JTL\DB\ReturnType;
use JTL\Shop;
use Plugin\jtl_ust_changer\Validation\DbValidator;

/**
 * Class ChangeVAT
 * @package Plugin\jtl_ust_changer
 */
class ChangeVAT
{
    /** @var Config */
    private $config;

    /** @var CurrentProcess */
    private $currentProcess;

    /** @var DbValidator */
    private $validator;

    /**
     * ChangeVAT constructor.
     */
    public function __construct()
    {
        $container       = Shop::Container();
        $this->config    = $container->get(Config::class);
        $this->validator = $container->get(DbValidator::class);
    }

    /**
     * Updates the Database with new vat-config and updates prices when configured.
     * @return void
     * @throws InvalidArgumentException|Exception
     */
    public function updateDB(): void
    {
        if ($this->config->isMaintenanceOn() === false) {
            $message = __('Wartungsmodus ist ausgeschaltet!');

            throw new Exception($message);
        }

        $this->currentProcess = $this->buildCurrentProcessConfig();
        
        $this->updateTsteuersatz();
        $this->updateTartikelVAT();

        // if gross prices should be updated:
        if ($this->currentProcess->isKeepGrossPrice() === true) {
            $currentProcessVAT = new CurrentProcessVAT(
                $this->currentProcess->getUstValue()->getOldVAT(),
                $this->currentProcess->getUstValue()->getNewVAT()
            );
            $this->updateTartikelPrice($currentProcessVAT);
            $this->updateTpreisdetail($currentProcessVAT);
            $this->updateTsonderpreise($currentProcessVAT);
        }

        $this->processEnd();
    }

    /**
     * Checks if there is an unfinished Database-Update and finish it or creates a new config for the current run.
     * @return CurrentProcess
     * @throws InvalidArgumentException|Exception
     */
    private function buildCurrentProcessConfig(): CurrentProcess
    {
        $process = $this->getCurrentProcessFromDb() ?? $this->newCurrentProcess();

        $ustValue       = new UStValue($process['oldVAT'], $process['newVAT']);
        $currentProcess = new CurrentProcess($ustValue);
        $currentProcess->setHistoryId($process['historyId'])
            ->setStartTime($process['startTime'])
            ->setKeepGrossPrice($process['keepGrossPrice']);

        return $currentProcess;
    }

    /**
     * @return array<string, mixed>|null
     * @throws Exception
     */
    private function getCurrentProcessFromDb(): ?array
    {
        $currentProcessDb = $this->config->getCurrentProcessFromDB();
        if ($currentProcessDb) {
            $currentProcessDb = $this->validator->validateCurrentProcess($currentProcessDb);
            return $currentProcessDb;
        }

        return null;
    }

    /**
     * @return array<string, mixed>
     * @throws Exception
     */
    private function newCurrentProcess(): array
    {
        $ustValue    = $this->config->getUstValue();
        $checkOldVAT = (int)$this->config->db->queryPrepared(
            'SELECT count(sts.fSteuersatz) as `count` FROM tsteuersatz sts 
                        INNER JOIN tsteuerzoneland stzl ON stzl.kSteuerzone = sts.kSteuerzone
                        WHERE stzl.cISO = :iso AND sts.fSteuersatz = :oldVAT;',
            [
                'iso' => 'DE',
                'oldVAT' => $ustValue->getOldVAT()
            ],
            ReturnType::SINGLE_ASSOC_ARRAY
        )['count'];
        if ($checkOldVAT === 0) {
            $message = \sprintf(
                __('Keine Einträge mit der Ust %s%% für Deutschland vorhanden.'),
                $ustValue->getOldVAT()
            );

            throw new InvalidArgumentException($message);
        }
        if ($checkOldVAT > 1) {
            $message = \sprintf(
                __('Mehr als ein Eintrag mit der Ust %s%% für Deutschland vorhanden.'),
                $ustValue->getOldVAT()
            );

            throw new InvalidArgumentException($message);
        }

        $now    = (new \DateTimeImmutable())->format('Y-m-d H:i:s');
        $insert = $this->config->db->queryPrepared(
            'INSERT INTO xplugin_jtl_ust_changer_history (startTime, oldVAT, newVAT, keepGrossPrice)
                   VALUES(:startTime, :oldVAT, :newVAT, :keepGrossPrice)',
            [
                'startTime'      => $now,
                'oldVAT'         => $ustValue->getOldVAT(),
                'newVAT'         => $ustValue->getNewVAT(),
                'keepGrossPrice' => $this->config->isKeepGrossPrice()
            ],
            ReturnType::AFFECTED_ROWS
        );

        if ($insert !== 1) {
            $message = 'DB Insert ' . __('fehlgeschlagen') . ': ChangeVAT()->newCurrentProcess().';

            throw new Exception($message);
        }

        $historyId = $this->config->db->queryPrepared(
            'SELECT historyId FROM xplugin_jtl_ust_changer_history ORDER BY historyId DESC LIMIT 1',
            [],
            ReturnType::SINGLE_ASSOC_ARRAY
        )['historyId'];
        $historyId = $this->validator->validateHistoryId($historyId);

        return [
            'historyId'      => $historyId,
            'newVAT'         => $ustValue->getNewVAT(),
            'oldVAT'         => $ustValue->getOldVAT(),
            'startTime'      => $now,
            'keepGrossPrice' => $this->config->isKeepGrossPrice()
        ];
    }

    /**
     * Update vat in table tsteuersatz
     */
    private function updateTsteuersatz(): void
    {
        $this->config->db->queryPrepared(
            'UPDATE tsteuersatz sts 
                    INNER JOIN tsteuerzoneland stzl ON stzl.kSteuerzone = sts.kSteuerzone
                    SET sts.fSteuersatz = :newVAT 
                WHERE sts.fSteuersatz = :oldVAT AND stzl.cISO = :iso',
            [
                'newVAT' => $this->currentProcess->getUstValue()->getNewVAT(),
                'oldVAT' => $this->currentProcess->getUstValue()->getOldVAT(),
                'iso' => 'DE'
            ],
            ReturnType::AFFECTED_ROWS
        );
    }

    /**
     * Sets the finished-Datetime for current run in history-table and unsets the config.
     * @return void
     * @throws Exception
     */
    private function processEnd(): void
    {
        $update = $this->config->db->queryPrepared(
            'UPDATE xplugin_jtl_ust_changer_history SET endTime = NOW() WHERE historyID = :historyID',
            ['historyID' => $this->currentProcess->getHistoryId()],
            ReturnType::AFFECTED_ROWS
        );
        if ($update !== 1) {
            $message = 'DB Update ' . __('fehlgeschlagen') . ': ChangeVAT()->processEnd().';

            throw new Exception($message);
        }
        unset($this->currentProcess);
    }

    /**
     * update vat-config in tartikel.
     * @return void
     */
    private function updateTartikelVAT(): void
    {
        $this->config->db->queryPrepared(
            'UPDATE tartikel SET fMwSt = :newVAT WHERE fMwSt = :oldVAT',
            [
                'newVAT' => $this->currentProcess->getUstValue()->getNewVAT(),
                'oldVAT' => $this->currentProcess->getUstValue()->getOldVAT()
            ],
            ReturnType::AFFECTED_ROWS
        );
    }

    /**
     * update product-prices in tartikel.
     *
     * @param CurrentProcessVAT $vats
     *
     * @return void
     */
    private function updateTartikelPrice(CurrentProcessVAT $vats): void
    {
        $this->config->db->queryPrepared(
            'UPDATE tartikel a
                SET a.fStandardpreisNetto = (ROUND(a.fStandardpreisNetto * :oldVAT2, 2) / :newVAT2) 
                WHERE a.fMwSt = :newVAT',
            [
                'oldVAT2' => $vats->getOldVAT2(),
                'newVAT2' => $vats->getNewVAT2(),
                'newVAT'  => $vats->getNewVAT()
            ],
            ReturnType::AFFECTED_ROWS
        );
    }

    /**
     * update product-prices in tpreisdetail.
     * @param CurrentProcessVAT $vats
     * @return void
     */
    private function updateTpreisdetail(CurrentProcessVAT $vats): void
    {
        $countPluginTpreisdetail =
            (int)$this->config->db->queryPrepared(
                'SELECT count(*) as `count` FROM xplugin_jtl_ust_changer_tpreisdetail
                        WHERE historyId = :historyID',
                ['historyID' => $this->currentProcess->getHistoryId()],
                ReturnType::SINGLE_ASSOC_ARRAY
            )['count'];
        if ($countPluginTpreisdetail === 0) {
            $this->config->db->queryPrepared(
                'INSERT INTO xplugin_jtl_ust_changer_tpreisdetail 
                        (kPreisDetail, kPreis, nAnzahlAb, fVKNetto, historyId)
                        (SELECT tpd.kPreisDetail,
                                tpd.kPreis,
                                tpd.nAnzahlAb,
                                tpd.fVKNetto,
                                :historyId
                            FROM tpreisdetail tpd)',
                ['historyId' => $this->currentProcess->getHistoryId()],
                ReturnType::AFFECTED_ROWS
            );
        }
        $stmnt =
            'UPDATE xplugin_jtl_ust_changer_tpreisdetail pd 
                JOIN tpreis p ON pd.kPreis = p.kPreis 
                JOIN tkundengruppe kg ON kg.kKundengruppe = p.kKundengruppe 
                JOIN tartikel a ON p.kArtikel = a.kArtikel 
            SET pd.fVKNetto = (ROUND(pd.fVKNetto * :oldVAT2, 2) / :newVAT2), 
                pd.modified = now() 
            WHERE kg.nNettoPreise = 0 
                AND pd.modified IS NULL 
                AND pd.historyId = :historyId
                AND a.fMwSt = :newVAT';
        $this->config->db->queryPrepared(
            $stmnt,
            [
                'oldVAT2'   => $vats->getOldVAT2(),
                'newVAT2'   => $vats->getNewVAT2(),
                'historyId' => $this->currentProcess->getHistoryId(),
                'newVAT'    => $vats->getNewVAT()
            ],
            ReturnType::AFFECTED_ROWS
        );

        $this->config->db->queryPrepared(
            'UPDATE tpreisdetail tpd
                    JOIN xplugin_jtl_ust_changer_tpreisdetail ptpd ON tpd.kPreisDetail = ptpd.kPreisDetail 
                    SET tpd.fVKNetto = ptpd.fVKNetto
                    WHERE ptpd.modified IS NOT NULL AND ptpd.historyId = :historyId',
            ['historyId' => $this->currentProcess->getHistoryId()],
            ReturnType::AFFECTED_ROWS
        );
    }

    /**
     * updates prices in tartikelsonderpreis
     * @param CurrentProcessVAT $vats
     * @return void
     */
    private function updateTsonderpreise(CurrentProcessVAT $vats): void
    {
        $countPluginTartikelsonderpreis = (int)$this->config->db->queryPrepared(
            'SELECT count(*) AS `count` FROM xplugin_jtl_ust_changer_tsonderpreise WHERE historyId = :historyId',
            ['historyId' => $this->currentProcess->getHistoryId()],
            ReturnType::SINGLE_ASSOC_ARRAY
        )['count'];

        if ($countPluginTartikelsonderpreis === 0) {
            $this->config->db->queryPrepared(
                'INSERT INTO xplugin_jtl_ust_changer_tsonderpreise
                       (kArtikelSonderpreis, kKundengruppe, fNettoPreis, historyId)
                       (SELECT tas.kArtikelSonderpreis,
                               tas.kKundengruppe,
                               tas.fNettoPreis,
                               :historyId
                       FROM tsonderpreise tas)',
                ['historyId' => $this->currentProcess->getHistoryId()],
                ReturnType::AFFECTED_ROWS
            );
        }
        $this->config->db->queryPrepared(
            'UPDATE xplugin_jtl_ust_changer_tsonderpreise ptsp 
                    JOIN tartikelsonderpreis asp ON asp.kArtikelSonderpreis = ptsp.kArtikelSonderpreis 
                    JOIN tartikel a ON a.kArtikel = asp.kArtikel 
                    JOIN tkundengruppe kg ON kg.kKundengruppe = ptsp.kKundengruppe 
                SET ptsp.fNettoPreis = (ROUND(ptsp.fNettoPreis * :oldVAT2, 2) / :newVAT2), 
                    ptsp.modified = now()
                WHERE kg.nNettoPreise = 0 
                  AND a.fMwSt = :newVAT 
                  AND (asp.nIstDatum = 0 OR asp.dEnde >= CURDATE())
                  AND ptsp.modified IS NULL',
            ['oldVAT2' => $vats->getOldVAT2(), 'newVAT2' => $vats->getNewVAT2(), 'newVAT' => $vats->getNewVAT()],
            ReturnType::AFFECTED_ROWS
        );

        $this->config->db->queryPrepared(
            'UPDATE tsonderpreise tsp
                        JOIN xplugin_jtl_ust_changer_tsonderpreise ptsp 
                            ON tsp.kArtikelSonderpreis = ptsp.kArtikelSonderpreis 
                                   AND tsp.kKundengruppe = ptsp.kKundengruppe
                    SET tsp.fNettoPreis = ptsp.fNettoPreis
                    WHERE ptsp.modified IS NOT NULL AND ptsp.historyId = :historyId;',
            ['historyId' => $this->currentProcess->getHistoryId()],
            ReturnType::AFFECTED_ROWS
        );
    }
}
